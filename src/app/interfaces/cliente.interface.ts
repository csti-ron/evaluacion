
export interface ClienteInterface {
    id?: string;
    name?: string;
    lastname?: string;
    age?: number;
    birthdate?: any;
}
