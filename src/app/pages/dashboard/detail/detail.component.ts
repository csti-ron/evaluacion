import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ClienteInterface } from 'src/app/interfaces/cliente.interface';
import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  is_load: boolean = false;

  is_submit: boolean = false;

  cliente: ClienteInterface = {};

  is_new: boolean = false;

  constructor(
    private readonly clienteService: ClienteService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {

    this.is_new = this.route.snapshot.data['is_new'];

    if (this.is_new) {
      this.loadNew();
    } else {
      const id = this.route.snapshot.paramMap.get('id');
      this.load(id!);
    }


  }

  loadNew() {
    this.cliente = this.clienteService.generateNew();

    this.is_load = true;
  }

  load(id: string) {

    this.clienteService.get(id).subscribe(c => {

      c.birthdate = moment(c.birthdate, 'x').format("YYYY-MM-DD"); //cambiamos el formato timestamp
      this.cliente = c;
      this.is_load = true;

    });

  }

  /**
   * Enviamos el formilario
   * @param f 
   */
  onSubmit(f: NgForm) {

    this.is_submit = true;

    if (!f.valid) { //sino es valido retornamos
      return;
    }


    const cliente_to_save: ClienteInterface = {
      name: this.cliente.name,
      lastname: this.cliente.lastname,
      birthdate: moment(this.cliente.birthdate).format('x')
    }

    if (this.is_new) {
      this.clienteService.save(cliente_to_save);
    } else {
      this.clienteService.update(cliente_to_save, this.cliente.id!);
    }

    this.router.navigate(["/dashboard"]);

  }


  /**
   * Actualizamos la edad segun el cambio de la fecha de nacimiento
   */
  onChangeBirthdate() {

    this.cliente.age = moment().diff(this.cliente.birthdate, 'years');

  }


}
