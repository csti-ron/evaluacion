import { Component, OnInit } from '@angular/core';
import { ClienteInterface } from 'src/app/interfaces/cliente.interface';
import { ClienteService } from 'src/app/services/cliente.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  clientes: ClienteInterface[] = [];

  average: number = 0;

  desviation: number = 0;

  constructor(
    private readonly clienteService: ClienteService,
  ) { }

  ngOnInit(): void {

    this.clienteService.getAll().subscribe((clientes) => {

      this.clientes = clientes;

      if (clientes.length) {
        this.average = this.getAverage(clientes.map(c => c.age));
        this.desviation = this.getStandardDeviation(clientes.map(c => c.age))
      }

    });

  }


  /**
   * Obtiene la desviación estandar
   * @param data Lista de números a evaluar
   */
  private getStandardDeviation(data: number[]) {

    let avg = this.getAverage(data);

    let squareDiffs = data.map(function (value: any) {
      let diff = value - avg;
      let sqrDiff = diff * diff;
      return sqrDiff;
    });

    let avgSquareDiff = this.getAverage(squareDiffs);

    return Math.sqrt(avgSquareDiff);
  }


  /**
   * Obtiene el promedio
   * @param data Lista de números a evaluar
   */
  private getAverage(data: number[]) {
    const sum = data.reduce((previous: any, current: any) => current += previous);

    const average = sum! / data.length;

    return average;
  }


}