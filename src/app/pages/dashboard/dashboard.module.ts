import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { FormsModule } from '@angular/forms';

//componentes
import { DashboardComponent } from './dashboard.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'list', component: ListComponent },  //pagina del listado de clientes
      { path: 'detail', redirectTo: '/dashboard/view', pathMatch: 'full' },  //pagina del detalle de un cliente sino envia un id redirecciona al dashboard
      { path: 'detail/:id', component: DetailComponent },  //pagina del detalle de un cliente se envia el id del clietne
      { path: 'add', component: DetailComponent, data: { is_new: true } },  //pagina para agregar un cliente
      { path: '', redirectTo: '/dashboard/list', pathMatch: 'full' } //redireccionar si esta en blanco
    ]
  }
];


@NgModule({
  declarations: [
    DashboardComponent,
    DetailComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ]
})
export class DashboardModule { }
