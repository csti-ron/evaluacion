import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClienteInterface } from '../interfaces/cliente.interface';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private clientesDB: AngularFireList<ClienteInterface>;

  constructor(
    private readonly db: AngularFireDatabase
  ) {

    this.clientesDB = this.db.list('/clientes', (ref) =>
      ref.orderByChild('name')
    );

  }


  /**
   * Generamos la data necesaria para un nuevo cliente
   */
  public generateNew(): ClienteInterface {
    return {
      age: 0
    };
  }



  /**
   * Guardamos el cliente
   * @param cliente 
   */
  public save(cliente: ClienteInterface) {
    return this.clientesDB.push(cliente);
  }

  public get(cliente_id: string) {
    // return this.db.object(`clientes/${cliente_id}`).snapshotChanges().toPromise();
    return this.db.object(`clientes/${cliente_id}`).snapshotChanges().pipe(
      map((data: any) => {
        console.log('data', data);
        const cliente: ClienteInterface = data.payload.val();
        return {
          id: data.payload.key,
          ...cliente,
          age: moment().diff(moment(cliente.birthdate, 'x'), 'years')
        };
      })
    );
  }


  /**
   * Obtenemos el listado de clientes
   */
  public getAll(): Observable<any[]> {

    return this.clientesDB.snapshotChanges().pipe(

      map((changes) => {

        return changes.map((data) => {

          const cliente: any = data.payload.val();

          return {
            id: data.payload.key,
            ...cliente,
            age: moment().diff(moment(cliente.birthdate, 'x'), 'years')
          }

        });

      })

    );
  }


  /**
   * Actualizamos el cliente
   * @param cliente 
   * @param cliente_id Id del cliente a modificar
   */
  public update(cliente: ClienteInterface, cliente_id: string) {
    return this.db.list('/clientes').update(cliente_id, cliente);
  }

}
