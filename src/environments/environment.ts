// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBTfy1InhRqMUI5XMASWXOp9GOSY4ZT-0o",
    authDomain: "csti-ron.firebaseapp.com",
    databaseURL: "https://csti-ron-default-rtdb.firebaseio.com",
    projectId: "csti-ron",
    storageBucket: "csti-ron.appspot.com",
    messagingSenderId: "638383033799",
    appId: "1:638383033799:web:e755eff9e6019b7dad9de2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
